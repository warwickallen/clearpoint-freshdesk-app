RESULTS_PER_PAGE = 30
MAXIMUM_NUMBER_OF_RESULTS_TO_FETCH = 5000
DEFAULT_COMPANY = 'GDT'

techOpsReviewTicketsApp = angular.module( 'techOpsReviewTicketsApp', ['ngSanitize', 'ngAnimate', 'btford.markdown'] )

.factory( 'fields', ->
    class Field
        constructor:(@name, @fd_field, @get = @get, @getForSort = @getForSort, @getForSelect = @getForSort) ->
        get:        (ticket) ->
            ticket[@fd_field]
        getForSort: (ticket) ->
            v = @get(ticket)
            v = if typeof v is 'string' then v.toLowerCase() else v
            #console.log @name + "=" + v
            v

    fields = {
        Agent:
            new Field 'Agent',
            'responder_name'
        CreationDate:
            new Field 'Creation Date',
            'created_at'
        Description:
            new Field 'Description',
            'description',
            (t) -> t.description_html,
            (t) -> t.description
        DueDate:
            new Field 'Due Date',
            'custom_field',
            (t) -> if t.custom_field.due_date_13303 then t.custom_field.due_date_13303 else ''
        ID:
            new Field 'ID',
            'display_id'
        InProgress:
            new Field 'In Progress',
            'custom_field',
            (t) -> t.custom_field.in_progress_13303
        Owner:
            new Field 'Owner',
            'requester_name'
        Priority:
            new Field 'Priority',
            'priority',
            (t) -> t.priority_name,
            (t) -> (t.priority << 13) - ((t.custom_field.priority_order_13303 or 8) << 10) - (t.custom_field.relative_priority_13303 ? 1000),
            (t) -> t.priority_name.toLowerCase(),
        PriorityOrder:
            new Field 'Priority Order',
            'custom_field',
            (t) -> (t.custom_field.priority_order_13303 or 4),
        RelativePriority:
            new Field 'Relative Priority',
            'custom_field',
            (t) -> t.custom_field.relative_priority_13303 ? '',
            (t) -> t.custom_field.relative_priority_13303 ? 1000,
        Status:
            new Field 'Status',
            'status_name'
        Subject:
            new Field 'Subject',
            'subject'
        TargetEnvironment:
            new Field 'Target Environment',
            'custom_field',
            (t) -> t.custom_field.target_environment_13303
        TechOpsMeetingNotes:
            new Field 'Tech Ops Meeting Notes',
            'custom_field',
            (t) -> t.custom_field.tech_ops_meeting_notes_13303
        Type:
            new Field 'Type',
            'ticket_type'
    }
    for key, f of fields
        fields[key.toLowerCase()] = f
        name = f.name
        unless name is key
            fields[name] = f
            fields[name.toLowerCase()] = f
    fields
)
.filter( 'classify',     -> (text)                       -> String(text).replace /[^A-Za-z0-9_-]/mg, '-' )
.filter( 'range',        -> (input, count)               -> [0 .. count - 1] )
.filter( 'orderByFields', ['fields', (fields) -> (tickets, order_by)  -> tickets? and tickets.sort? and tickets.sort (a, b) ->
                                return null unless order_by? and tickets?
                                for order in order_by when !cmp and (f = fields[order.field])?
                                    cmp = switch
                                        when f.getForSort(a) > f.getForSort(b) then +1
                                        when f.getForSort(a) < f.getForSort(b) then -1
                                        else 0
                                    cmp = -cmp unless order.ascending
                                cmp
] )
.filter( 'quickFilter', ['fields', (fields) -> (tickets, quick_filter_terms, selectorFunction) ->
                            return null unless selectorFunction? and quick_filter_terms? and tickets?
                            re_terms = (new RegExp term, 'i' for term in quick_filter_terms.split(/\s+/) when term? and term.length)
                            selected = []
                            if re_terms.length
                                for ticket in tickets
                                    matched = true
                                    try matched = selectorFunction fields, ticket
                                    if matched  # Don't bother with the quick search terms if the selectorFunction returned false
                                        for re_term in re_terms
                                            for field_name, field of fields
                                                break if matched = re_term.test field.getForSort ticket # OR over fields
                                            break if !matched   # AND over search terms
                                        selected.push ticket if matched and selectorFunction fields, ticket
                            else
                                for ticket in tickets
                                    matched = true
                                    try matched = selectorFunction fields, ticket
                                    selected.push ticket if matched
                            return selected
] )

.controller( 'TechOpsReviewTicketsCtrl', [
    '$scope', '$http', '$sce', 'fields'
    ($scope, $http, $sce, fields) ->

        #### Preferences ####
        $scope.preferences =
            automatically_expand_company_notes:     no

        window.debug = $scope
        console.debug $scope: $scope
        $scope.messages = new Array # Each element will be of the form { class:<string>, show:<boolean>, text:<string> }
        sendMessage = (type, text) ->
            console.log type.toUpperCase()+': ', text
            message = { class:type, show:yes, text:text }
            $scope.messages.push message
            # Make the message disappear after 10 or 30 seconds.
            setTimeout (-> message.show = no), switch type
                when 'error' then 30000
                else              10000

        getURLParameter = (name) ->
            decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null,''])[1].replace(/\+/g, '%20')) or null

        getTickets = (filter_key, page = 1) ->
            $scope.tickets = [] if page is 1
            get = $http.get( "helpdesk/tickets.json?filter_key=#{filter_key}&page=#{page}" )
            get.success (data, status) ->
                        unless 0 of data and 'id' of data[0]
                            sendMessage 'error', data
                            return
                        for datum in data
                            $scope.tickets.push datum
                        unless $scope.preset? and $scope.preset.length or $scope.query_string? and $scope.query_string.length
                            # Change the select filter or query string unless one of them have already been changed.
                            if (param = getURLParameter 'preset')?
                                $scope.preset = param
                                temp_query_watcher = $scope.$watch 'query_string', ->
                                    temp_query_watcher()        # Stop watching.
                                    $scope.query_string = param if (param = getURLParameter 'query')?
                            else
                                $scope.query_string = param if (param = getURLParameter 'query')?
                        if data.length == RESULTS_PER_PAGE and $scope.tickets.length < MAXIMUM_NUMBER_OF_RESULTS_TO_FETCH
                            getTickets filter_key, page + 1
                        else
                            $scope.loading = no
            get.error   (error, status) ->
                        sendMessage 'error', "[#{status}] #{error}"
                        $scope.loading = no
        getCompanyDetails = (company) ->
            if $scope.preferences.automatically_expand_company_notes
                $scope.company.notes_collapsed = yes
            get = $http.get( "customers/#{company.id}.json" )
            get.success (data, status) ->
                        unless 'customer' of data
                            sendMessage 'error', data
                            return
                        for key, value of data.customer
                            company[key] = value
                        if $scope.preferences.automatically_expand_company_notes
                            $scope.company.notes_collapsed = no
            get.error   (error, status) ->
                        sendMessage 'error', "[#{status}] #{error}"

        companies = [
            { id: 71481,    short_name: 'GDT',      'note':null }
        ]
        $scope.company = do ->
            short_name = getURLParameter 'company'
            unless short_name?
                short_name = DEFAULT_COMPANY
                sendMessage 'warning', "No company was specified, so the default company &#8220;#{DEFAULT_COMPANY}&#8221; has been selected."
            for company in companies
                return company if company.short_name is short_name
            null
        $scope.company.notes_collapsed = yes

        # TODO: Automatically fetch this list from Freshdesk.
        $scope.freshdesk_custom_ticket_views =
            GDT: [
                { id: 246691,   name:   'Not closed'                                        }
                { id: 314684,   name:   'Not closed, High, Priority order 1 & 2'            }
                { id: 318928,   name:   'GDT Whitewall'                                     }
                { id: 314457,   name:   'Priority List'                                     }
                { id: 314015,   name:   'FreshDesk tickets that are internal to ClearPoint' }
                { id: 313845,   name:   'Open tickets raised by the support team'           }
                { id: 320146,   name:   'GDT Change Orders'                                 }
                { id: 313826,   name:   'Project tickets'                                   }
                { id: 311291,   name:   'Open and not low'                                  }
                { id: 293143,   name:   'Open reporting tickets'                            }
                { id: 308447,   name:   'All'                                               }
                { id: 315987,   name:   'open testing tickets'                              }
                { id: 320787,   name:   'GDT, Created within the last 6 months'             }
            ]
        $scope.freshdesk_custom_ticket_view = do ->
            default_view = $scope.freshdesk_custom_ticket_views[$scope.company.short_name][0]
            list = getURLParameter 'list'
            if list?
                for view in $scope.freshdesk_custom_ticket_views[$scope.company.short_name]
                    if list is view.id or list.toLowerCase() is view.name.toLowerCase()
                        default_view = view
                        break
            default_view

        $scope.loading = no
        $scope.tickets = []
        $scope.changeFreshdeskCustomTicketView = ->
            new
            window.history.pushState(
                {
                    # TODO
                },
                company.short_name + 'Tickets:' + freshdesk_custom_ticket_view.name,
                "/new-url",
            )
            $scope.refreshTickets()
        $scope.refreshTickets = ->
            $scope.loading = yes
            getTickets $scope.freshdesk_custom_ticket_view.id
            getCompanyDetails $scope.company
        $scope.refreshTickets()

        putSuccess = (data, status, headers, config) ->
            if typeof data is 'object' and 'helpdesk_ticket' of data
                console.log { SUCCESS: {data:data, status:status, headers:headers, config:config} }
                ticket = data.helpdesk_ticket
            else
                putError data, status, headers, config
            status
        putError = (data, status, headers, config) ->
            console.error { ERROR: {data:data, status:status, headers:headers, config:config} }
            status
        $scope.saveTicketField = (ticket, field, event) ->
            console.log saveTicketField: { ticket:ticket, field:field, event:event }
            text = event.target.value
            data = helpdesk_ticket:
                custom_field:
                    tech_ops_meeting_notes_13303: text
            console.log data, JSON.stringify data
            put = $http.put "/helpdesk/tickets/#{ticket.display_id}.json", data
            put.success putSuccess
            put.error   putError

        $scope.fields = fields
        $scope.unique_fields = {}
        $scope.unique_fields[field.name] = field for key, field of $scope.fields

        $scope.order_by = [
            { field: 'Type', ascending: true  }
            { field: 'ID',   ascending: false }
            { field: null,   ascending: true  }
            { field: 'ID',   ascending: false } # This is a hidden element, included to ensure a deterministic sort order.
        ]

        $scope.presets =
            '':
                order_by:       []
                query_string:   ''
            'Whitewall':
                order_by:       [
                                    { field: 'Priority',ascending: false }
                                ]
                query_string:   '(`priority` is high and `priority order` is 1 or `relative priority` < 100) and `status` isn\'t resolved'
            'Incidents and High-Priority Problems and Service Requests':
                order_by:       [
                                    { field: 'Type',    ascending: true  }
                                    { field: 'Priority',ascending: false }
                                ]
                query_string:   '(`Type` is incident or `Type` is in Problem, "Service Request" and `Priority` is in High, Urgent) and `Status` isn\'t in Resolved, Closed'
            'Priority':
                order_by:       [
                                    { field: 'Priority',ascending: false }
                                ]
                query_string:   '`Status` isn\'t in resolved, closed, or "tier-3 support" and (`Priority` is in high, or urgent or `Type` is incident)'
            'Pending A.T.':
                query_string:   '`Tech Ops Meeting Notes` contains "Pending A.T."'
        $scope.preset = ''
        $scope.$watch 'preset', (new_preset, old_preset) ->
            #console.debug "Changing preset: #{old_preset} -> #{new_preset}"
            preset = $scope.presets[new_preset]
            return null unless preset?
            for i in [0 .. $scope.order_by.length - 2] # Don't change the hidden element.
                $scope.order_by[i] = preset.order_by? and preset.order_by[i] ? { field: null, ascending: true }
            $scope.query_string = preset.query_string
            null

        $scope.quick_filter = ''

        $scope.query_string = ''
        $scope.selectorFunction = (d) -> true
        $scope.$watch 'query_string', (new_query, old_query) ->
            # TODO: Wrap the "safe apply" inside a $timeout, as explained in
            #       http://stackoverflow.com/questions/20263118/what-is-phase-in-angularjs/21611534#21611534
            if new_query.length
                #console.log "Calling the parser to generate a function from:\n" + new_query
                try
                    $scope.selectorFunction = parser.parse new_query
                    $scope.query_error = undefined
                    $scope.$apply() unless $scope.$$phase
                catch error
                    $scope.query_error = error.toString().replace(/^Error: Parse error on line \d+:\n/,'')
            else
                $scope.selectorFunction = (d) -> true
                $scope.query_error = undefined
                $scope.$apply() unless $scope.$$phase
            #console.debug "$scope.query_error = #{$scope.query_error}"
        $scope.show_selector_function = false

        # Users
        $scope.users = []
        loadUser = (user_id) ->
            unless user_id of $scope.users
                get = $http.get "contacts/#{user_id}.json"
                get.success (data) -> $scope.users[user_id] = data.user
                get.error (data, status) -> [ { status: status } ]
            true

        # Notes
        $scope.showHideNotes = (ticket) ->
            ticket._show_notes = !ticket._show_notes
            if ticket._show_notes
                unless ticket._notes_retrieved
                    console.log "Retrieving notes ..."
                    get = $http.get "helpdesk/tickets/#{ticket.display_id}.json"
                    get.success (data) ->
                        ticket.notes = (loadUser(note.note.user_id) and note.note for note in data.helpdesk_ticket.notes when not (note.note.private or note.note.deleted))
                        ticket._notes_retrieved = true
                    get.error (data, status) -> [ { status: status } ]
] )
