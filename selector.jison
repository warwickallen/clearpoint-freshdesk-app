/* description: "Selector" query language definition. */

/* author: Warwick Allen */


/* Lexical Grammar */
/*******************/

%lex

%options flex case-insensitive

%%

/* Grouping, Conjunctions and Modifiers */
"("                                         return 'OPEN_GROUP'                 ;
")"                                         return 'CLOSE_GROUP'                ;
not(?!\w)                                   return 'NOT'
and(?!\w)                                   return 'AND'                        ;
or(?!\w)                                    return 'OR'                         ;

/* Operators */
is(\s+no|n\')t\s+greater\s+than(?!\w)       return 'NOT_GREATER'                ;
"<="                                        return 'NOT_GREATER'                ;
(is\s+)?greater\s+than(?!\w)                return 'GREATER'                    ;
">"                                         return 'GREATER'                    ;
is(\s+no|n\')t\s+less\s+than(?!\w)          return 'NOT_LESS'                   ;
">="                                        return 'NOT_LESS'                   ;
(is\s+)\?less\s+than(?!\w)                  return 'LESS'                       ;
"<"                                         return 'LESS'                       ;

does(\s+no|n\')t\s+begin\s+with(?!\w)       return 'NOT_BEGINS'                 ;
"!^"                                        return 'NOT_BEGINS'                 ;
begins\s+with(?!\w)                         return 'BEGINS'                     ;
"^"                                         return 'BEGINS'                     ;
does(\s+no|n\')t\s+contain(?!\w)            return 'NOT_CONTAINS'               ;
"!~"                                        return 'NOT_CONTAINS'               ;
(matche|contain)s(?!\w)                     return 'CONTAINS'                   ;
"~"                                         return 'CONTAINS'                   ;
does(\s+no|n\')t\s+end\s+with(?!\w)         return 'NOT_ENDS'                   ;
"!$"                                        return 'NOT_ENDS'                   ;
ends\s+with(?!\w)                           return 'ENDS'                       ;
"$"                                         return 'ENDS'                       ;

is(\s+no|n\')t\s+(in|a\s+member\s+of)(?!\w) return 'NOT_IN'                     ;
(is\s+)?(in|a\s+member\s+of)(?!\w)          return 'IN'                         ;

is(\s+no|n\')t(\s+equal\s+to)?(?!\w)        return 'NOT_EQUALS'                 ;
does(\s+no|n\')t\s+equal(?!\w)              return 'NOT_EQUALS'                 ;
"!="                                        return 'NOT_EQUALS'                 ;
is(\s+equal\s+to)?(?!\w)                    return 'EQUALS'                     ;
equals(?!\w)                                return 'EQUALS'                     ;
"=="                                        return 'EQUALS'                     ;

/* Fields */
\`[^\`]+\`                                  return 'FIELD'                      ;

/* Operands */
\"(\\.|[^\\"])*\"                           return 'DOUBLE_QUOTED_STRING'       ;
\'(\\.|[^\\\'])*\'                          return 'SINGLE_QUOTED_STRING'       ;
\,(\s+?or|\s+?and)?(?!\w)                   return 'LIST_SEPARATOR'             ;
[\w.+-]+(?!\w)                              return 'WORD'                       ;

/* Ignore */
\s+                                                                             ;

<<EOF>>                                     return 'EOF'                        ;

/lex


/* Operator Associations and Precedence */
/****************************************/

%left   'OR'
%left   'AND'
%left   'EQUALS' 'GREATER' 'LESS' 'BEGINS' 'ENDS' 'IN' 'CONTAINS'
%left   'NOT'


/* Language Grammar */
/********************/

%start  programme

%%

programme:              query EOF                                           { return new Function('f','d', 'try {return ' + $1 + '} catch (e) {return null}') }
                    ;
query:                  condition
                    |   OPEN_GROUP query CLOSE_GROUP AND query              { $$ = "(" + $2 + ") && " + $5 }
                    |   OPEN_GROUP query CLOSE_GROUP OR  query              { $$ = "(" + $2 + ") || " + $5 }
                    |   OPEN_GROUP query CLOSE_GROUP                        { $$ = "(" + $2 + ")" }
                    |   NOT query                                           { $$ = "!" + $2 }
                    |   condition AND query                                 { $$ = $1 + " && " + $3 }
                    |   condition OR  query                                 { $$ = $1 + " || " + $3 }
                    ;

condition:              operand     EQUALS    operand                       { $$ = "(" + $1 + " ==" + $3 + ")"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand NOT_EQUALS    operand                       { $$ = "(" + $1 + "!==" + $3 + ")"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand     GREATER   operand                       { $$ = "(" + $1 + " > " + $3 + ")"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand NOT_GREATER   operand                       { $$ = "(" + $1 + " <=" + $3 + ")"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand     LESS      operand                       { $$ = "(" + $1 + " < " + $3 + ")"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand NOT_LESS      operand                       { $$ = "(" + $1 + " >=" + $3 + ")"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand     BEGINS    operand                       { $$ =  "(new RegExp('^'+" + $3 +     ",'i').test(" + $1 + "))"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand NOT_BEGINS    operand                       { $$ = "!(new RegExp('^'+" + $3 +     ",'i').test(" + $1 + "))"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand     CONTAINS  operand                       { $$ =  "(new RegExp("     + $3 +     ",'i').test(" + $1 + "))"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand NOT_CONTAINS  operand                       { $$ = "!(new RegExp("     + $3 +     ",'i').test(" + $1 + "))"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand     ENDS      operand                       { $$ =  "(new RegExp("     + $3 + "+'$','i').test(" + $1 + "))"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand NOT_ENDS      operand                       { $$ = "!(new RegExp("     + $3 + "+'$','i').test(" + $1 + "))"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand     IN        list                          { $$ = "(" + $3.map(function(i) {return $1 +  "==" + i}).join(" || ") + ")"; /* console.log("CONDITION("+$$+")") */ }
                    |   operand NOT_IN        list                          { $$ = "(" + $3.map(function(i) {return $1 + "!==" + i}).join(" && ") + ")"; /* console.log("CONDITION("+$$+")") */ }
                    ;

list:                   operand LIST_SEPARATOR list                         { $$ = $3; $$.unshift($1); /* console.log("LIST("+$$+")") */ }
                    |   operand                                             { $$ = [$1];               /* console.log("LIST("+$$+")") */ }
                    ;

operand:                WORD                                                { try {$$ = eval($1)} catch (e) {$$ = "'" + $1.toLowerCase() + "'"};                /* console.log("OPERAND("+$$+")") */ }
                    |   DOUBLE_QUOTED_STRING                                { $$ = $1.toLowerCase();                                                            /* console.log("OPERAND("+$$+")") */ }
                    |   SINGLE_QUOTED_STRING                                { $$ = $1.toLowerCase();                                                            /* console.log("OPERAND("+$$+")") */ }
                    |   FIELD                                               { $$ = "f['" + $1.substring(1, $1.length - 1).toLowerCase() + "'].getForSelect(d)"; /* console.log("OPERAND("+$$+")") */ }
                    ;
