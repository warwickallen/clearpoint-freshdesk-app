#!/bin/bash

# This script should be launched by the bootstrap script.

set -x

apt-get update
apt-get install -y      \
    nodejs              \
    npm                 \
    coffeescript        \
    inotify-tools       \

# Update Node.js
npm install -g n
n stable
nodejs -v

npm install -g          \
    bower               \
    jison               \
    less                \

cd /home/ubuntu/clearpoint-freshdesk-app

npm install --save minimist     # Required for the HTTP server.

# Initial compilation of Jison files.
for f in *.jison
do
    n=$(echo -n "$f" | sed 's/\.jison$//')
    echo "Compiling $n.jison"
    sudo -uubuntu /usr/local/bin/jison "$n.jison"
done

# Initial compilation of Less files.
for f in *.less
do
    n=$(echo -n "$f" | sed 's/\.less$//')
    echo "Compiling $n.less"
    /usr/local/bin/lessc --source-map="$n.css.map" "$n.less" | sudo -uubuntu tee "$n.css" | head
done

# Install JavaScript libraries.
sudo -uubuntu bower install     \
    angular                     \
    angular-sanitize            \
    angular-animate             \
    angular-markdown-directive

# Start the Upstart services.
for i in        \
    coffee      \
    jison       \
    less        \
    http
do
    cp init/$i.conf /etc/init/
    service $i start
done
