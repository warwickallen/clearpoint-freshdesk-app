healthCheckApp = angular.module('healthCheckApp', [])

.controller 'HealthCheckCtrl', [
    '$scope', '$http'
    ($scope, $http) ->
        $scope.query = 'helpdesk/tickets/5250.json'
        $http.get($scope.query)
        .success (data, status) ->
            $scope.status = status
            $scope.data = JSON.stringify data, null, "    "
        .error   (data) ->
            process.exit status
]
