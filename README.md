## Set Up

(on a Debian-based OS)

* Install the Node Package Manager.  E.g.:

        sudo apt-get install npm


* Install CoffeeScript and set it to automatically update the JavaScript
file.

        npm install -g coffee-script coffee -cmwb \
            tech-ops-review-tickets.coffee


* Install the Less and Jison compilers.

        npm install -g less jison


* Copy the service configuration files in clearpoint-freshdesk-app / init to /etc/init.d.  


* Start the services.
   `for service in coffee jison less html; do
     sudo service $service start;
   done`


Browse the ticket list at
`http://localhost:8000/tech-ops-review-tickets.html` (if you haven't
changed the port number from the default.