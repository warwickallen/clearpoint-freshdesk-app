#!/bin/bash

cat <<HERE >/home/ubuntu/bootstrap.sh
#!/bin/bash

set -x

apt-get -y install git

sudo -uubuntu mkdir -pv /home/ubuntu/.ssh

sudo -uubuntu tee /home/ubuntu/.ssh/id_rsa <<PRIVATE_KEY >/dev/null
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAn+eBgsgb/otNCjSZIs80YZ0xZruYKB1Z2CIqDeUmtuAR8MEJ
qV4UgZkiDsF5xiRQuUJwNY+lVuyCpsBhFEoyV90sSxI9Zfwn0C73gVUIB1nwg2t5
nsNgcOpFKveZBq3Wdu3QX/gBtMiTI8yaW7iPPU8pLxLH3dsZQXVyB8n4dDZABPcO
asJXMXSWCQCNNPxLn2PMvCxRTahrjFMiKqEhOpWhhgGr1crW6QUQ88vzX/Skk7JO
L4fuX1EnOTz5Bepcy9O7jfiWr1lE5Vd+/M0tMytwMo4km8v/2nfSNvsRYXMfRK9I
xWuDYE9TwENXEw/5uszwKRRe6nlXLxom+dVsywIDAQABAoIBACZFHPz08aZBJqYL
op7vImU+XuVsfmUTaT5OJux+bH6WMBUDzlEnwBaoVLtdWMywQlWkbgCTqYmdP0Oz
hUh63YOj3ob1DZbL+6dapkfg1vQTdXmujkQOFJXRq841xf0QO+To09c0HwxqeLS3
sjXDfRy6RmwKvaT+XBQLjMgKcZk2Rb/+EKjOHHFlrf4RKsNkkc3C+jvsO9rLhlFP
IZNabGe/akyx+IREUuy4c0RFfFGMogmF8K/I3e/eT161sVbxpCKKHWiVz16aVRSQ
/7O3+AudriCaB0pNpCxLvPJodDuJWrvvNrtE8nkhXG0sLrkJ62ugghdisuOa0eWc
jIis/3kCgYEA0g4L0uDHFJm1ouoyvD/RRLLf/WUaQkVgBjkV8ZRLKWq3eC2jpMYG
WYgYdLAFKYHG4pLBRTDOz5axx6gzbiJg6McB3tammLGo2a/ve1XDrcFAmKdsMXPR
sYMNVtqB0Th2mC3PkOSmG17f0qin0Tfkwmqjh8gUyM9FiYe4Olku3r8CgYEAwuFL
z8FMVyY6pIVdVRoyG3ePUQfv5t13PHWSX2e9CoedwbxQjxy+30HCvxUqtuK3hD3B
UxDQfzbiqYjTnbaWm13ZOVQEjFa+P4Sg9HpvsIN/+V98ogR2swuKH/33c/bSSJ93
yh4L71c9Tp4zFZxaluHD9X6sifl8vDMFP9zVwPUCgYA63mLj68j/dgCYsugbKAt/
JF5wBqiwT/8UX2VN+hCCf/0BOlE1eaYH64NnGHmp40wY7aYqvP4I4aPbX8FiKVJd
ioB0slWoT4oR0CPeve+EP7fZ0Hoa9embxS203LIrL5bVGWmlot8GprU1tbyqfu6m
qfpAsItap/RGEfpIp/vhwwKBgDfBOW1hAjRUzaZQOZmlevx8wlYOjUZhykFT6eK9
zUn77tiExg+Ly9gX+l2s8qGMT7DX8j2Pq/cZftO+P7z5hjFBMmrCbOKx1pBZo6sO
dHHtzWlNNl0sJcGn5YatpGRbE2KPrTf4TLXXIcUiYb8nEovkX6oW6co3xMYbWr8g
sU8ZAoGBAMck5fGI3Q4qA7fTJB0YZWhq9oDSjRwKBWQjUuqTuVsLrh2trzNdPJrG
CnSag63KTYaHix1l8tMPXUILl2aQFaKrVmR03lGkaud61i1mGdFmHX9YTkiq2OvS
zlKe8JbArNplcPmNPgKbuM70tHyxmFTbvJtdV/U+bxlDglpcMyyP
-----END RSA PRIVATE KEY-----
PRIVATE_KEY
chmod -v 400 /home/ubuntu/.ssh/id_rsa

sudo -uubuntu tee /home/ubuntu/.ssh/id_rsa.pub <<PUBLIC_KEY
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCf54GCyBv+i00KNJkizzRhnTFmu5goHVnYIioN5Sa24BHwwQmpXhSBmSIOwXnGJFC5QnA1j6VW7IKmwGEUSjJX3SxLEj1l/CfQLveBVQgHWfCDa3mew2Bw6kUq95kGrdZ27dBf+AG0yJMjzJpbuI89TykvEsfd2xlBdXIHyfh0NkAE9w5qwlcxdJYJAI00/EufY8y8LFFNqGuMUyIqoSE6laGGAavVytbpBRDzy/Nf9KSTsk4vh+5fUSc5PPkF6lzL07uN+JavWUTlV378zS0zK3AyjiSby//ad9I2+xFhcx9Er0jFa4NgT1PAQ1cTD/m6zPApFF7qeVcvGib51WzL clearpoint-freshdesk
PUBLIC_KEY

sudo -uubuntu tee -a /home/ubuntu/.ssh/config <<DISABLE_HOST_KEY_CHECK
Host bitbucket.org
    StrictHostKeyChecking no
    UserKnownHostsFile=/dev/null
DISABLE_HOST_KEY_CHECK

sudo -uubuntu git clone git@bitbucket.org:warwickallen/clearpoint-freshdesk-app.git /home/ubuntu/clearpoint-freshdesk-app

/home/ubuntu/clearpoint-freshdesk-app/boot.sh
HERE

chmod -v 744 /home/ubuntu/bootstrap.sh
/home/ubuntu/bootstrap.sh 2>&1 | tee /home/ubuntu/bootstrap.log
