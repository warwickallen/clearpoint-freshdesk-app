argv    = require('minimist')(process.argv.slice 2)
http    = require 'http'
url     = require 'url'
fs      = require 'fs'
path    = require 'path'

class Options
    @_opts = []
    constructor: (@_opts...) ->
        for opt, i in @_opts
            for key in ['name', 'letter']
                eval "this[opt.#{key}] = function(value) {
                        if (value != null) { this._opts[#{i}].value = value }
                        return this._opts[#{i}].value }"

opt = new Options
        name:           'port'
        letter:         'p'
        description:    "The port for the HTTP server to listen on."
        value:          8000
    ,
        name:           'home'
        letter:         'h'
        description:    "The page to serve if the path is '/'."
        value:          'tech-ops-review-tickets.html'
    ,
        name:           'domain'
        letter:         'd'
        description:    "The FreshDesk endpoint (domain)."
        value:          'clearpoint.freshdesk.com'
    ,
        name:           'key'
        letter:         'k'
        description:    "The FreshDesk API key.  You can find this by browsing to your profile page in the FreshDesk console."
        value:          'LrNezyqFEK0VTp6i8hfo'
        hide_default:   true
    ,
        name:           'verbose'
        letter:         'v'
        description:    "Enable verbose output."
        value:          false

if argv.help or argv.h
    console.log """
        Usage: node #{path.basename process.argv[1]} [OPTIONS]
        Launches a simple HTTP server that serves static requests and forwards helpdesk requests to FreshDesk.
        Options:
          -h  --help  Prints this help message and exits.
    """ + ( for o in opt._opts
        "\n  -#{o.letter}  --#{o.name}  #{o.description}" + if o.hide_default then '' else "  Defaults to #{o.value}."
    ).join ''
    process.exit 0

# Update options from the command-line arguments.
opt[arg] value for arg, value of argv when arg isnt '_'

if opt.verbose()
    console.log opt._opts

sendFile = (response, path) ->
    if /\.(png|svg|ico|gif|jpe?g)/i.test path
        console.log "Sending the image '#{path}'." if opt.v()
        fs.readFile path, 'binary', (err, data) ->
            if err
                console.error "Error reading #{path}: #{err}"
            else
                response.writeHead 200, {'Content-Type': 'image/' + path.substring path.lastIndexOf('.') + 1 }
                response.write data, 'binary'
                response.end()
    else
        console.log "Sending the contents of '#{path}'." if opt.v()
        fs.readFile path, 'utf8', (err, data) ->
            if err
                console.error "Error reading #{path}: #{err}"
            else
                response.write data
                response.end()

errorResponse = (response, message) ->
    #sendFile my_response, path.join __dirname, 'tickets.json'   # Send a local version of the ticket data (for development use).
    console.error message
    response.writeHead 500
    response.write message
    response.end()

http.createServer( (incoming_request, my_response) ->
    console.log "Received request:", incoming_request if opt.v()
    path_part = url.parse(incoming_request.url).pathname
    if path_part is '/'
        console.log 'Serving the default landing page: ' + opt.home()
        path_part += opt.home()
    path_full = path.normalize path.join __dirname, path_part
    query = url.parse(incoming_request.url).query
    if /\/(helpdesk|customers|contacts)\//.test path_part
        request_options =
            host:   opt.domain()
            path:   if query? then "#{path_part}?#{query}" else path_part
            auth:   opt.key() + ':X'
            method: incoming_request.method
        console.log "Creating request: " + JSON.stringify request_options if opt.v()
        request_to_remote = new http.ClientRequest request_options
        console.log request: request_to_remote if opt.v()
        request_to_remote.on 'error', (error) ->
            errorResponse my_response, "Client Request error: " + error
        request_to_remote.on 'response', (freshdesk_response) ->
            data = ''
            freshdesk_response.on 'data', (chunk) ->
                data += chunk
            freshdesk_response.on 'end', ->
                my_response.write data
                my_response.end()
                console.log my_response if opt.v()
            freshdesk_response.on 'error', (error) ->
                errorResponse my_response, "Error in receiving data from #{opt.domain()}: " + error
        request_to_remote.end()
    else
        sendFile my_response, path_full
).listen opt.port()
